# AcronymsApp #

This application aims to show the definitions of an acronym and its respective variants

### Specs  ###

* This project runs with min iOS version 14.0 

### Instalation  ###

* Please clone the project with command: 
```
git clone https://alvarocorp@bitbucket.org/alvarocorp/acronymsapp.git
```
* open file `AcronymsApp.xcworkspace` with XCode > v12
* Change certificate if necessary in project configuration
* Run the project
