//
//  ContentView.swift
//  AcronymsApp
//
//  Created by Alvaro Cofre Perez on 12-05-21.
//

import SwiftUI

struct AcronymsMainView: View {
    
    @StateObject var viewModel = AcronymsViewModel()
    
    var body: some View {
        
        VStack(spacing: 15){
            
            // Search Bar...
            Group(){
                HStack(spacing: 10){
                    
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(.gray)
                    
                    TextField("Search Acronyms", text: $viewModel.searchQuery)
                        .modifier(TextFieldClearButton(text: $viewModel.searchQuery))
                        .autocapitalization(.none)
                    
                }
                .padding(.vertical,10)
                .padding(.horizontal, 20)
                .background(Color(.systemGray6))
                .cornerRadius(8)
            }
            .padding(.horizontal, 20)
            
            ScrollView(){
                if viewModel.isError {
                    VStack(){
                        Spacer()
                            .frame(width: 0, height: UIScreen.main.bounds.height/5)
                        Image(systemName: "xmark.octagon.fill")
                            .resizable()
                            .frame(width: 100, height: 100, alignment: .center)
                        Text("An error has ocurred, please try again later")
                            .font(.title2)
                            .bold()
                            .multilineTextAlignment(.center)
                    }
                    .foregroundColor(.red)
                    
                }
                else if viewModel.isLoading {
                    Spacer()
                        .frame(width: 0, height: UIScreen.main.bounds.height/5)
                    ProgressView("Loading results")
                    
                } else {
                    
                    if viewModel.fetchedDictionaryResponse.isEmpty
                        && !viewModel.searchQuery.isEmpty
                        && viewModel.isCompleted {
                        VStack(){
                            Spacer()
                                .frame(width: 0, height: UIScreen.main.bounds.height/5)
                            Image(systemName: "xmark.octagon.fill")
                                .resizable()
                                .frame(width: 100, height: 100, alignment: .center)
                            Text("No results found!")
                                .font(.title2)
                                .bold()
                                .multilineTextAlignment(.center)
                        }
                        .foregroundColor(.gray)
                    } else {
                        VStack(alignment: .leading, spacing: 20.0) {
                            ForEach(viewModel.fetchedDictionaryResponse) { acromine in
                                Text("We found \(acromine.lfs.count) results for \(acromine.sf)")
                                    .font(.title)
                                    .bold()
                                
                                ForEach(acromine.lfs){ lf in
                                    AcronimeRow(lf: lf)
                                        .padding(.trailing, 20)
                                    Divider()
                                }
                                
                            }
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, 20)

                    }
                }
            }
            .frame(maxWidth: .infinity)
        }
    }
}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        AcronymsMainView()
    }
}
