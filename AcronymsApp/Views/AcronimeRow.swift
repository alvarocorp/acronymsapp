//
//  AcronimeRow.swift
//  AcronymsApp
//
//  Created by Alvaro Cofre Perez on 13-05-21.
//

import SwiftUI

struct AcronimeRow: View {
    let lf: Longform

    var body: some View {
        VStack(alignment: .leading){
            Text(lf.lf)
                .font(.title2)
                .fontWeight(.bold)
            
            if !lf.vars.isEmpty {
                DisclosureGroup("See variations ...", content: {
                    ForEach(lf.vars) { oneVar in
                        Text(oneVar.lf)
                            .frame(maxWidth:.infinity, alignment: .leading)
                            .font(.body)
                            .foregroundColor(.gray)
                    }
                })
                .font(.body)

            }
            
        }
        .frame(maxWidth:.infinity, alignment: .leading)
    }
}

struct AcronimeRow_Previews: PreviewProvider {
    static var previews: some View {
        let mockData = Longform(
            lf: "implosive therapy",
            freq: 3,
            since: 1978,
            vars: [
                Variation(
                    lf: "implosive therapy",
                    freq: 2,
                    since: 1998
                ),
                Variation(
                    lf: "Implosive Therapy",
                    freq: 1,
                    since: 1978
                )
            ]
        )
        AcronimeRow(lf: mockData)
    }
}
