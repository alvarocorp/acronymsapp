//
//  AcronymsViewModel.swift
//  AcronymsApp
//
//  Created by Alvaro Cofre Perez on 12-05-21.
//

import SwiftUI
import Foundation
import Alamofire
import SwiftPrettyPrint
import Combine


class AcronymsViewModel: ObservableObject {
    @Published var searchQuery = ""
    var searchCancellable: AnyCancellable? = nil
    
    @Published var isLoading: Bool = false
    @Published var isCompleted: Bool = false
    @Published var isError: Bool = false
    @Published var fetchedDictionaryResponse: [DictionaryResponse] = []
    
    init(){
        searchCancellable = $searchQuery
            .removeDuplicates()
            // we need to avoid fetch for every typing....
            // so it will wait for 0.4 after user ends typing...
            .debounce(for: 0.4, scheduler: RunLoop.main)
            .sink(receiveValue: { str in
                self.isError = false
                self.isCompleted = false
                
                if str == ""{
                    self.fetchedDictionaryResponse.removeAll()
                }
                else{
                    self.isLoading = true
                    self.fetchedDictionaryResponse.removeAll()
                    self.getDictionaryResponse(sf: str, {  val in
                        print(val)
                    })
                }
            })
    }
    
    func getDictionaryResponse(sf: String, _ completion:@escaping (Bool) -> ()) {
        isLoading = true
        isCompleted = false
        
        let parameters: Parameters = ["sf":sf]
        
        
        AF.request("http://www.nactem.ac.uk/software/acromine/dictionary.py",
                   method: HTTPMethod.get,
                   parameters: parameters,
                   encoding: URLEncoding.default)
            
            .responseDecodable(of: [DictionaryResponse].self){ response in
                switch response.result {
                case .success(let value):
                    self.fetchedDictionaryResponse = value
                    self.isCompleted = true
                    completion(true)
                    Pretty.prettyPrint(value)
                case .failure(let error):
                    self.isError = true
                    completion(true)
                    print(error)
                }
                
                self.isLoading = false
            }
//            .cURLDescription { description in
//                print(description)
//            }        
    }
}
