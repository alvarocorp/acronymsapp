//
//  AcronymsAppApp.swift
//  AcronymsApp
//
//  Created by Alvaro Cofre Perez on 12-05-21.
//

import SwiftUI

@main
struct AcronymsAppApp: App {
    var body: some Scene {
        WindowGroup {
            AcronymsMainView()
        }
    }
}
