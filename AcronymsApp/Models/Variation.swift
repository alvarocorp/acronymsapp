//
//  Variation.swift
//  AcronymsApp
//
//  Created by Alvaro Cofre Perez on 12-05-21.
//

import Foundation

struct Variation : Codable {
    var lf: String
    var freq: Int
    var since: Int
}

extension Variation : Identifiable {
    var id: UUID? { return UUID() }
}

extension Variation : Equatable {}
