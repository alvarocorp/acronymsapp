//
//  DictionaryResponse.swift
//  AcronymsApp
//
//  Created by Alvaro Cofre Perez on 12-05-21.
//

import Foundation

struct DictionaryResponse :  Codable {
    var sf: String
    var lfs: [Longform]
}


extension DictionaryResponse : Identifiable {
    var id: UUID? { return UUID() }
}

extension DictionaryResponse : Equatable {}
