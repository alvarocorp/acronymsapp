//
//  Longform.swift
//  AcronymsApp
//
//  Created by Alvaro Cofre Perez on 12-05-21.
//

import Foundation

struct Longform: Codable {
    var lf: String
    var freq: Int
    var since: Int
    var vars: [Variation]
}

extension Longform : Identifiable {
    var id: UUID? { return UUID() }
}

extension Longform : Equatable {}
