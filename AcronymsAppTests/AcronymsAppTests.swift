//
//  AcronymsAppTests.swift
//  AcronymsAppTests
//
//  Created by Alvaro Cofre Perez on 12-05-21.
//

import XCTest
import Alamofire
import Mocker

@testable import AcronymsApp

class AcronymsAppTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testMockGetDictionaryResponse() throws{
        let configuration = URLSessionConfiguration.af.default
        configuration.protocolClasses = [MockingURLProtocol.self] + (configuration.protocolClasses ?? [])
        let sessionManager = Session(configuration: configuration)
        let apiEndpoint = URL(string: "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=HMM")!
        
        let expectedDictionaryResponse: [DictionaryResponse] = [
            DictionaryResponse(
                sf: "HMMA",
                lfs: [
                    Longform(
                        lf: "4-hydroxy-3-methoxymethamphetamine",
                        freq: 43,
                        since: 1967,
                        vars: [
                            Variation(
                                lf: "4-hydroxy-3-methoxymethamphetamine",
                                freq: 21,
                                since: 1996
                            ),
                            Variation(
                                lf: "4-hydroxy-3-methoxymandelic acid",
                                freq: 14,
                                since: 1967
                            ),
                            Variation(
                                lf: "4-hydroxy-3-methoxy mandelic acid",
                                freq: 2,
                                since: 1977
                            ),
                            Variation(
                                lf: "4-hydroxy-3-methoxy-mandelic acid",
                                freq: 2,
                                since: 1979
                            ),
                            Variation(
                                lf: "4-hydroxy 3-methoxymandelic acid",
                                freq: 2,
                                since: 1980
                            ),
                            Variation(
                                lf: "4-Hydroxy-3-methoxymandelic acid",
                                freq: 1,
                                since: 1978
                            ),
                            Variation(
                                lf: "4-Hydroxy-3-methoxymethamphetamine",
                                freq: 1,
                                since: 2005
                            )
                        ]
                    )
                ]
            )
        ]
        let requestExpectation = expectation(description: "AcronymsAppTests")
        
        let mockedData = try! JSONEncoder().encode(expectedDictionaryResponse)
        let mock = Mock(url: apiEndpoint, dataType: .json, statusCode: 200, data: [.get: mockedData])
        mock.register()
        
        sessionManager
            .request(apiEndpoint)
            .responseDecodable(of: [DictionaryResponse].self) { (response) in
                XCTAssertNil(response.error)
                XCTAssertEqual(response.value, expectedDictionaryResponse)
                requestExpectation.fulfill()
            }.resume()

        wait(for: [requestExpectation], timeout: 10.0)
    }
    
    func testGetDictionaryEmptyResponse() throws {
        let requestExpectation = expectation(description: "AcronymsAppTests")

        let vm = AcronymsViewModel()
        vm.getDictionaryResponse(sf: "A"){ val in
            requestExpectation.fulfill()
        }
        
        wait(for: [requestExpectation], timeout: 10.0)

        XCTAssertEqual(vm.fetchedDictionaryResponse, [])
    }

    func testGetDictionarySuccessResponse() throws {
        let requestExpectation = expectation(description: "AcronymsAppTests")
        let expectedDictionaryResponse: [DictionaryResponse] = [
            DictionaryResponse(
                sf: "HMMA",
                lfs: [
                    Longform(
                        lf: "4-hydroxy-3-methoxymethamphetamine",
                        freq: 43,
                        since: 1967,
                        vars: [
                            Variation(
                                lf: "4-hydroxy-3-methoxymethamphetamine",
                                freq: 21,
                                since: 1996
                            ),
                            Variation(
                                lf: "4-hydroxy-3-methoxymandelic acid",
                                freq: 14,
                                since: 1967
                            ),
                            Variation(
                                lf: "4-hydroxy-3-methoxy mandelic acid",
                                freq: 2,
                                since: 1977
                            ),
                            Variation(
                                lf: "4-hydroxy-3-methoxy-mandelic acid",
                                freq: 2,
                                since: 1979
                            ),
                            Variation(
                                lf: "4-hydroxy 3-methoxymandelic acid",
                                freq: 2,
                                since: 1980
                            ),
                            Variation(
                                lf: "4-Hydroxy-3-methoxymandelic acid",
                                freq: 1,
                                since: 1978
                            ),
                            Variation(
                                lf: "4-Hydroxy-3-methoxymethamphetamine",
                                freq: 1,
                                since: 2005
                            )
                        ]
                    )
                ]
            )
        ]

        let vm = AcronymsViewModel()
        vm.getDictionaryResponse(sf: "HMMA"){ val in
            requestExpectation.fulfill()
        }
        
        wait(for: [requestExpectation], timeout: 10.0)

        XCTAssertEqual(vm.fetchedDictionaryResponse, expectedDictionaryResponse)
    }
    
    func testGetDictionaryErrorResponse() throws {
        let requestExpectation = expectation(description: "AcronymsAppTests")

        let vm = AcronymsViewModel()
        vm.getDictionaryResponse(sf: "😎"){ val in
            requestExpectation.fulfill()
        }
        
        wait(for: [requestExpectation], timeout: 10.0)

        XCTAssertEqual(vm.isError, true)
    }

}
